extern crate curl;

use std::time::{Duration, Instant};

use curl::easy::Easy;

pub fn duration_to_seconds(dt: Duration) -> f64 {
    let n_secs = dt.as_secs() as f64;
    let n_nanosecs = dt.subsec_nanos() as f64 / 10u32.pow(9) as f64;
    n_secs + n_nanosecs
}

#[derive(Debug)]
struct BwScanner {
    client: Easy
}

impl BwScanner {
    pub fn new(socks_port: u16) -> Self {
        let proxy_addr = format!("socks5://127.0.0.1:{}", socks_port);
        let mut client = Easy::new();        
        client.proxy(&proxy_addr).unwrap();
        BwScanner { client }
    }

    pub fn run(&mut self) {
        let base_url = "http://bwauth.torproject.org/bwauth.torproject.org".to_string();        
        let file_sizes = vec![2, 4, 8, 16, 32, 64];

        for file_size in file_sizes {
            println!("\nFetching {}M...", file_size);

            let url = format!("{}/{}M", base_url, file_size);
            let (n_bytes_received, dt) = self.fetch(&url);

            // Bandwidth is reported in units of kilobytes per second, kB/s, as
            // this is the unit used by the descriptors to report relay bandwidth.
            let bw = n_bytes_received as f64 / 1024.0 / dt;
            
            println!("N BYTES RECEIVED => {}", n_bytes_received);
            println!("SECONDS ELAPSED => {}", dt);
            println!("BANDWIDTH (kB/s) => {}\n", bw);
        }
    }

    pub fn fetch(&mut self, url: &str) -> (usize, f64) {
        self.client.url(url).unwrap();
        let mut n_bytes_received = 0;
        let timer = Instant::now();

        {
            let mut transfer = self.client.transfer();
            
            transfer.write_function(|data| {
                let chunk_length = data.len();
                n_bytes_received += chunk_length;
                Ok(chunk_length)
            }).unwrap();

            transfer.perform().unwrap();
        } 

        let dt = duration_to_seconds(timer.elapsed());
        (n_bytes_received, dt)
    }
}

fn main() {
    let mut scanner = BwScanner::new(9050);
    scanner.run();
}
